import os
import sys
import setuptools
from wheel.bdist_wheel import bdist_wheel as _bdist_wheel


with open("README.rst") as fp:
    long_description = fp.read()


setuptools.setup(
    name="windows_entry_exe",
    url="https://gitlab.com/alelec/windows_entry_exe/",
    zip_safe=True,
    author="Andrew Leech",
    author_email="andrew@alelec.net",
    description=("Replacement exe launchers for setuptools entry_points"),
    long_description=long_description,
    license="MIT",
    packages=["windows_entry_exe"],
    entry_points="""
        [distutils.setup_keywords]
        use_windows_entry_exe = windows_entry_exe:enable
    """,
    python_requires=">=3.2",
    include_package_data=True,
    use_scm_version=True,
    setup_requires=['setuptools-scm'],
)